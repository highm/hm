# high man

[pm2 docs](https://pm2.io/doc/en/runtime/guide/ecosystem-file/)

初始化部署

```bash
pm2 deploy production setup
```

更新

```bash
pm2 deploy production update
```

启动服务

```bash
pm2 deploy production exec "pm2 start --only hm"
```

指定环境：

```bash
pm2 deploy production exec "pm2 start --only hm --env production"
```
