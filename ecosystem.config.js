module.exports = {
  apps: [{
    name: 'hm',
    script: 'index.js',
    env: {
      NODE_ENV: 'development',
    },
    env_production: {
      NODE_ENV: 'production',
    },
  }],
  deploy: {
    // "production" is the environment name
    production: {
      env: {
        NODE_ENV: 'production',
      },
      // SSH key path, default to $HOME/.ssh
      key: '/Users/micro-ttang/.ssh/gcp',
      // SSH user
      user: 'ttang',
      // SSH host
      host: ['hm.atory.cc'],
      // SSH options with no command-line flag, see 'man ssh'
      // can be either a single string or an array of strings
      ssh_options: 'StrictHostKeyChecking=no',
      // GIT remote/branch
      ref: 'origin/master',
      // GIT remote
      repo: 'https://gitlab.com/highm/hm.git',
      // path in the server
      path: '/home/ttang/hm',
      // Pre-setup command or path to a script on your local machine
      'pre-setup': 'apt-get install git ; ls -la',
      // Post-setup commands or path to a script on the host machine
      // eg: placing configurations in the shared dir etc
      'post-setup': 'ls -la',
      // pre-deploy action
      'pre-deploy-local': "echo 'This is a local executed command'",
      // post-deploy action
      'post-deploy': 'npm install',
    },
  },
};
