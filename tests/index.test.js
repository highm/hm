const request = require('supertest');

const app = require('../index');

// close the server after each test
afterEach(() => {
  app.close();
});

describe('routes: index', () => {
  test('should respond as expected', async () => {
    const response = await request(app.callback()).get('/');
    expect(response.status).toEqual(200);
    // expect(response.type).toEqual("application/json");
    // expect(response.body.data).toEqual("Sending some JSON");
  });
});
