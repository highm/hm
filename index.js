const Koa = require('koa');
const Router = require('koa-router');
const config = require('config');
const apm = require('elastic-apm-node');

const packageInfo = require('./package.json');

const serviceName = packageInfo.name.toUpperCase();
const serviceVersion = packageInfo.version;

const PORT = config.get('PORT');
const app = new Koa();
const router = new Router();

const isProduction = process.env.NODE_ENV === 'production';
let requestTimes = 0;

// eslint-disable-next-line no-unused-vars
app.on('error', (err, ctx) => {
  /* centralized error handling:
   *   console.log error
   *   write error to log file
   *   save error and request information to database if ctx.request match condition
   *   ...
  */
  if (isProduction) {
    apm.captureError(err);
    apm.endTransaction('', new Date().getTime());
  } else {
    console.error(err, ctx.method, ctx.path);
  }
});

app.use(async (ctx, next) => {
  if (isProduction) apm.startTransaction(ctx.path, 'router', { startTime: new Date().getTime() });
  try {
    await next();
    if (isProduction) apm.endTransaction(ctx.res.statusCode, new Date().getTime());
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = isProduction ? '.>_<.' : err.message;
    ctx.app.emit('error', err, ctx);
  } finally {
    requestTimes += 1;
  }
});

router.get('/', (ctx) => {
  ctx.body = `${serviceName}`;
});

router.get('/error', () => {
  throw new Error('err hapend');
});

router.get('/koaerr', (ctx) => {
  ctx.throw(404, 'not found');
});

router.get('/check', (ctx) => {
  ctx.body = `
    ${serviceName}-${serviceVersion} in ${process.env.NODE_ENV}
    requestTimes: ${requestTimes}
    apm:
      version: ${apm.version}
      isStarted: ${apm.isStarted()}
  `;
});

app.use(router.routes()).use(router.allowedMethods());

app.close = () => {
  console.log('close some dependence service');
};

if (process.env.NODE_ENV !== 'test') {
  app.listen(PORT, () => {
    if (isProduction) {
      apm.start({
        serviceName,
        serviceVersion,
        serverUrl: config.get('APM.SERVER_URL'),
      });
    }

    console.log(`${new Date()}: ${serviceName}-${serviceVersion} start at ${PORT} in ${process.env.NODE_ENV}`);
  });
} else {
  module.exports = app;
}
